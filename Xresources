! colors {{{

! gruvbox {{{

#define  gb_dark0_hard      #1d2021
#define  gb_dark0           #282828
#define  gb_dark0_soft      #32302f
#define  gb_dark1           #3c3836
#define  gb_dark2           #504945
#define  gb_dark3           #665c54
#define  gb_dark4           #7c6f64

#define  gb_gray_245        #928374
#define  gb_gray_244        #928374

#define  gb_light0_hard     #f9f5d7
#define  gb_light0          #fbf1c7
#define  gb_light0_soft     #f2e5bc
#define  gb_light1          #ebdbb2
#define  gb_light2          #d5c4a1
#define  gb_light3          #bdae93
#define  gb_light4          #a89984

#define  gb_bright_red      #fb4934
#define  gb_bright_green    #b8bb26
#define  gb_bright_yellow   #fabd2f
#define  gb_bright_blue     #83a598
#define  gb_bright_purple   #d3869b
#define  gb_bright_aqua     #8ec07c
#define  gb_bright_orange   #fe8019

#define  gb_neutral_red     #cc241d
#define  gb_neutral_green   #98971a
#define  gb_neutral_yellow  #d79921
#define  gb_neutral_blue    #458588
#define  gb_neutral_purple  #b16286
#define  gb_neutral_aqua    #689d6a
#define  gb_neutral_orange  #d65d0e

#define  gb_faded_red       #9d0006
#define  gb_faded_green     #79740e
#define  gb_faded_yellow    #b57614
#define  gb_faded_blue      #076678
#define  gb_faded_purple    #8f3f71
#define  gb_faded_aqua      #427b58
#define  gb_faded_orange    #af3a03

! }}}
! globals {{{

*background:  gb_dark0
*foreground:  gb_light1
*color0:      gb_dark0
*color8:      gb_gray_245
*color1:      gb_neutral_red
*color9:      gb_bright_red
*color2:      gb_neutral_green
*color10:     gb_bright_green
*color3:      gb_neutral_yellow
*color11:     gb_bright_yellow
*color4:      gb_neutral_blue
*color12:     gb_bright_blue
*color5:      gb_neutral_purple
*color13:     gb_bright_purple
*color6:      gb_neutral_aqua
*color14:     gb_bright_aqua
*color7:      gb_light4
*color15:     gb_light1

! }}}

! }}}
! urxvt {{{

urxvt.internalBorder: 0
urxvt.borderColor: 0
urxvt.loginShell: true
urxvt.scrollBar: false
urxvt.cursorBlink: true
urxvt.urlLauncher: firefox
!urxvt.perl-ext-common: resize-font
urxvt.font: xft:fira code:size=11

! }}}
! rofi {{{

rofi.color-enabled:    true
rofi.bw:               0
rofi.separator-style:  solid
rofi.width:            30
rofi.font:             fira code 20
rofi.lines:            10
rofi.hide-scrollbar:   true
rofi.fixed-num-lines:  false

!                   bg                  border      separator
rofi.color-window:  gb_dark0,           gb_light1,  gb_light1
!                   bg                  fg          bg-alt              hl-bg              hl-fg
rofi.color-normal:  gb_dark0,           gb_light1,  gb_dark1,           gb_dark3,          gb_light1
rofi.color-active:  gb_neutral_yellow,  gb_dark0,   gb_neutral_yellow,  gb_bright_yellow,  gb_dark0
rofi.color-urgent:  gb_neutral_red,     gb_dark0,   gb_neutral_red,     gb_bright_red,     gb_dark0

! }}}
! vim: foldmethod=marker
