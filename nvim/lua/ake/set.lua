local g = vim.g
local o = vim.o
local indent = 2

g.mapleader = ' '
g.loaded_netrw = 1
g.loaded_netrwPlugin = 1

-- enable auto complete
-- o.omnifunc = 'v:lua.vim.lsp.omnifunc'

o.expandtab = true
o.shiftwidth = indent
o.tabstop = indent
o.smartindent = true

o.list = true
o.listchars = 'tab:▸ ,extends:❯,precedes:❮,nbsp:±,trail:⸱'
o.number = true
o.relativenumber = true
o.cursorline = true
-- o.cursorcolumn = true
o.wrap = false

o.swapfile = false
o.background = 'dark'
o.inccommand = 'nosplit'
o.hlsearch = true
o.incsearch = true

-- o.completeopt = 'menuone,noselect'
-- o.completeopt = 'longest,menuone'
o.completeopt = 'menu,menuone,noselect'
o.hidden = true
o.splitbelow = true
o.splitright = true

o.ignorecase = true
o.smartcase = true
o.equalalways = true
o.termguicolors = true
o.scrolloff = 20
o.timeoutlen = 300
o.cmdheight = 1
-- o.lazyredraw = false
o.updatetime = 300
o.foldlevel = 99
o.foldmethod = 'expr'
o.foldexpr = 'nvim_treesitter#foldexpr()'
o.showmode = false
o.signcolumn = 'yes'
o.mouse = nil

vim.api.nvim_create_autocmd({ 'termopen' }, { pattern = '*', command = 'startinsert' })
vim.api.nvim_create_autocmd({ 'vimresized' }, { command = 'wincmd =' })

vim.diagnostic.config({ virtual_text = false })

if os.getenv('SHELL') == '/usr/bin/fish' or vim.opt.shell == '/usr/bin/fish' then
  vim.opt.shell = '/bin/sh'
else
  vim.opt.shell = os.getenv('SHELL')
end
