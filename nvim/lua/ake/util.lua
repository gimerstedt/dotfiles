local M = {}

function M.dump(o)
  print(vim.inspect(o))
end

---checks for val in arr
---@generic T
---@param arr T[]
---@param val T
---@return boolean
function M.contains(arr, val)
  for _, v in pairs(arr) do
    if v == val then
      return true
    end
  end
  return false
end

---fix termcodes for term bindings
---@param str string
---@return string
function M.t(str)
  return vim.api.nvim_replace_termcodes(str, true, true, true)
end

---get filename of current buffer
---@return string
function M.get_curr_filename()
  return vim.fn.eval("expand('%:t')")
end

---get directory of current buffer
---@return string
function M.get_curr_file_dir()
  return vim.fn.eval("expand('%:p:h')")
end

---check if the current buffer is a terminal
---@return boolean
function M.is_buf_term()
  local buf_type = vim.fn.eval('&buftype')
  if buf_type == 'terminal' then
    return true
  end
  return false
end

---browse directory in new buffer in direction (e/sp/vsp)
---@param direction string
function M.open(direction)
  local cmd = 'e'
  local filename = M.get_curr_filename()
  local path = M.get_curr_file_dir()

  if direction ~= nil then
    cmd = direction
  end

  if M.is_buf_term() then
    path = '.'
  end

  vim.cmd(cmd .. ' ' .. path)

  if filename ~= nil then
    local search_cmd = 'call search(" ' .. filename .. '$")'
    vim.cmd(search_cmd)
  end
end

---checks if file exists on disk
---@param fname string
---@return boolean
function M.file_exists(fname)
  local stat = vim.loop.fs_stat(fname)
  return (stat and stat.type) or false
end

---toggle between spec/source
---@param opts ({ allowed_extensions?: string[], test_suffixes?: string[] })
function M.toggle_test_file(opts)
  opts = opts or {}
  local allowed_extensions = opts.allowed_extensions or { 'ts', 'js', 'tsx', 'jsx' }
  local test_suffixes = opts.test_suffixes or { 'spec', 'test' }

  local curr_filename = M.get_curr_filename()
  local path = M.get_curr_file_dir()

  local filename, fext = string.match(curr_filename, '^(.-)%.(.*)$')
  local test_name, test_suffix, test_ext = string.match(curr_filename, '^(.-)%.(.*)%.(.*)$')

  local ext = test_ext and test_ext or fext
  local name = test_name and test_name or filename

  if M.contains(allowed_extensions, ext) then
    local path_to_open

    if M.contains(test_suffixes, test_suffix) then
      path_to_open = path .. '/' .. name .. '.' .. ext
    else
      for _, test_suff in pairs(test_suffixes) do
        local test_path = path .. '/' .. name .. '.' .. test_suff .. '.' .. ext
        if M.file_exists(test_path) then
          path_to_open = test_path
          break
        end
      end
    end

    if path_to_open ~= nil then
      vim.cmd('e' .. ' ' .. path_to_open)
    else
      print('No test file :/')
    end
  end
end

---check if running on windows
---@return boolean
function M.is_windows()
  return vim.loop.os_uname() == 'Windows_NT'
end

---run function if able to load module
---@param module string
---@param fn fun(arg: any)
function M.if_module(module, fn)
  local status, mo = pcall(require, module)
  if status then
    fn(mo)
  end
end

return M
