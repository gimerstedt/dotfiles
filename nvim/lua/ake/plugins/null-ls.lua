return {
  'jose-elias-alvarez/null-ls.nvim',
  config = function()
    local nls = require('null-ls')

    local nls_formatting = nls.builtins.formatting
    local nls_diagnostics = nls.builtins.diagnostics
    local nls_code_actions = nls.builtins.code_actions

    nls.setup({
      debug = true,
      sources = {
        nls_formatting.stylua,
        nls_formatting.prettierd,
        -- nls_formatting.prettierd.with({
        --   condition = function(utils)
        --     local deno = { 'deno.json', 'deno.jsonc' }
        --     return not utils.has_file(deno) and not utils.root_has_file(deno)
        --   end,
        -- }),
        -- nls_formatting.deno_fmt.with({
        --   condition = function(utils)
        --     local deno = { 'deno.json', 'deno.jsonc' }
        --     return utils.has_file(deno) or utils.root_has_file(deno)
        --   end,
        -- }),
        nls_formatting.google_java_format,
        nls_diagnostics.eslint_d.with({
          condition = function(utils)
            local eslint = { '.eslintrc.json', '.eslintrc.js' }
            return utils.has_file(eslint) or utils.root_has_file(eslint)
          end,
        }),
        nls_code_actions.eslint_d,
      },
    })
  end,
}
