return {
  'nat-418/boole.nvim',
  keys = {
    '<c-a>',
    '<c-x>',
  },
  config = function()
    local boole = require('boole')
    vim.keymap.set('n', '<c-a>', function()
      boole.run('increment')
    end)
    vim.keymap.set('n', '<c-x>', function()
      boole.run('decrement')
    end)
  end,
}
