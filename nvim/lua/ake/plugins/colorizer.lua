return {
  'norcalli/nvim-colorizer.lua',
  config = function()
    -- #ffa / #faf / #aff / #faa / #afa / #aaf
    require('colorizer').setup({ '*' }, {
      RGB = true,
      RRGGBB = true,
      names = true,
      RRGGBBAA = true,
      rgb_fn = true,
      hsl_fn = true,
      css = true,
      css_fn = true,
    })
  end,
}
