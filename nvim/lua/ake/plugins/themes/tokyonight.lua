return {
  'folke/tokyonight.nvim',
  lazy = false,
  priority = 1000,
  config = function()
    require('tokyonight').setup({
      transparent = true,
    })
    vim.api.nvim_command('colorscheme tokyonight')
  end,
}
