return {
  'catppuccin/nvim',
  name = 'catppuccin',
  lazy = false,
  priority = 1000,
  config = function()
    local undercurl = { 'undercurl' }
    require('catppuccin').setup({
      integrations = {
        native_lsp = {
          underlines = {
            errors = undercurl,
            warnings = undercurl,
            information = undercurl,
            hints = undercurl,
          },
        },
      },
    })
    vim.api.nvim_command('colorscheme catppuccin')
  end,
}
