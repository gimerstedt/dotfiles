return {
  'EdenEast/nightfox.nvim',
  config = function()
    local ok, p = pcall(require, 'nightfox')
    if not ok then
      return
    end

    p.setup({
      options = {
        -- Compiled file's destination location
        compile_path = vim.fn.stdpath('cache') .. '/nightfox',
        compile_file_suffix = '_compiled', -- Compiled file suffix
        transparent = false, -- Disable setting background
        terminal_colors = true, -- Set terminal colors (vim.g.terminal_color_*) used in `:terminal`
        dim_inactive = true, -- Non focused panes set to alternative background
      },
    })
    vim.api.nvim_command('colorscheme duskfox')
  end,
}
