return {
  'VonHeikemen/lsp-zero.nvim',
  dependencies = {
    -- LSP Support
    'neovim/nvim-lspconfig',
    'williamboman/mason.nvim',
    'williamboman/mason-lspconfig.nvim',

    -- Autocompletion
    'hrsh7th/nvim-cmp',
    'hrsh7th/cmp-buffer',
    'hrsh7th/cmp-path',
    'saadparwaiz1/cmp_luasnip',
    'hrsh7th/cmp-nvim-lsp',
    'hrsh7th/cmp-nvim-lua',

    -- Snippets
    'L3MON4D3/LuaSnip',
    'rafamadriz/friendly-snippets',

    -- below custom added

    -- json schemas
    'b0o/SchemaStore.nvim',
  },
  config = function()
    require('mason.settings').set({
      ui = {
        border = 'rounded',
      },
    })

    local lsp = require('lsp-zero')
    lsp.preset('recommended')

    lsp.set_preferences({
      set_lsp_keymaps = { omit = { 'gr' } },
    })

    -- disable formatting + misc.
    lsp.configure('sumneko_lua', {
      settings = {
        Lua = {
          format = {
            enable = false,
          },
          runtime = {
            -- Tell the language server which version of Lua you're using (most likely LuaJIT in the case of Neovim)
            version = 'LuaJIT',
            -- Setup your lua path
            -- path = runtime_path,
          },
          diagnostics = {
            globals = { 'vim' },
          },
          -- here be dragons
          -- workspace = {
            -- Make the server aware of Neovim runtime files
            -- library = vim.api.nvim_get_runtime_file('', true),
          -- },
          telemetry = {
            enable = false,
          },
        },
      },
    })

    -- disable formatting
    lsp.configure('html', {
      on_attach = function(client, _)
        client.server_capabilities.documentFormattingProvider = false
        client.server_capabilities.documentRangeFormattingProvider = false
      end,
    })

    -- lsp.configure('jdtls', {
    --   init_options = {
    --     jvm_args = { '-javaagent:$HOME/.local/share/java/lombok.jar' },
    --   },
    -- })

    -- disable formatting
    lsp.configure('tsserver', {
      -- single_file_support = false,
      -- disable_commands = false,
      -- debug = true,
      on_attach = function(client, _)
        client.server_capabilities.documentFormattingProvider = false -- 0.8
        client.server_capabilities.documentRangeFormattingProvider = false -- 0.8
      end,
    })

    lsp.configure('jsonls', {
      settings = {
        json = {
          hover = true,
          completion = true,
          validate = true,
          schemas = require('schemastore').json.schemas(),
        },
      },
    })

    lsp.configure('yamlls', {
      settings = {
        yaml = {
          hover = true,
          completion = true,
          validate = true,
          schemas = require('schemastore').json.schemas(),
        },
      },
    })

    lsp.setup()
  end,
}
