return {
  'simeji/winresizer',
  cmd = 'WinResizerStartResize',
  config = function()
    vim.g.winresizer_keycode_cancel = 122
    vim.g.winresizer_keycode_finish = 113
  end,
}
