return {
  'lewis6991/gitsigns.nvim',
  dependencies = { 'nvim-lua/plenary.nvim' },
  event = 'BufReadPre',
  config = function()
    local gitsigns = require('gitsigns')
    gitsigns.setup({
      current_line_blame = true,
    })
    vim.keymap.set('n', '<leader>gb', function()
      gitsigns.blame_line()
    end)
  end,
}
