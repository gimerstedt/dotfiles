return {
  'tamago324/lir.nvim',
  dependencies = { 'nvim-lua/plenary.nvim', 'kyazdani42/nvim-web-devicons' },
  config = function()
    local lir = require('lir')
    local actions = require('lir.actions')
    local mark_actions = require('lir.mark.actions')
    local clipboard_actions = require('lir.clipboard.actions')

    local function selected_filename()
      local ctx = require('lir.vim').get_context()
      return string.gsub(ctx:current_value(), require('plenary.path').path.sep .. '$', '')
    end

    local function abs_path(name)
      return vim.api.nvim_buf_get_name(0) .. '/' .. name
    end

    local function open(args)
      local name = selected_filename()
      local ext = name:match('^.+(%..+)$')
      if ext == '.mkv' or ext == '.mp4' or ext == '.avi' then
        local source = abs_path(name)
        vim.notify('Playing: ' .. source)
        vim.api.nvim_command('!mpv ' .. source)
      elseif ext == '.png' or ext == '.jpg' or ext == '.jpeg' then
        local source = abs_path(name)
        vim.notify('Playing: ' .. source)
        vim.api.nvim_command('!feh ' .. source)
      else
        actions.edit(args)
      end
    end

    local function rename(args)
      local old = selected_filename()
      local ext = old:match('^.+(%..+)$')
      if ext == '.ts' or ext == '.tsx' then
        local source = abs_path(old)
        vim.ui.input({ prompt = 'New path: ', default = source }, function(input)
          if input == '' or input == source or input == nil then
            return
          end
          require('typescript').renameFile(source, input)
        end)
      else
        actions.rename(args)
      end
    end

    lir.setup({
      show_hidden_files = false,
      devicons = {
        enable = true,
      },
      mappings = {
        ['l'] = open,
        ['L'] = open,
        ['<cr>'] = open,

        ['<C-s>'] = actions.split,
        ['<C-v>'] = actions.vsplit,
        ['<C-t>'] = actions.tabedit,

        ['-'] = actions.up,
        ['h'] = actions.up,
        ['H'] = actions.up,
        ['q'] = actions.quit,

        ['d'] = actions.mkdir,
        ['D'] = actions.delete,
        ['%'] = actions.newfile,
        ['R'] = rename,
        ['@'] = actions.cd,
        ['Y'] = actions.yank_path,
        ['gh'] = actions.toggle_show_hidden,
        ['J'] = function()
          mark_actions.toggle_mark('n')
          vim.cmd('normal! j')
        end,

        ['C'] = clipboard_actions.copy,
        ['X'] = clipboard_actions.cut,
        ['P'] = clipboard_actions.paste,
      },
      hide_cursor = false,
    })
  end,
}
