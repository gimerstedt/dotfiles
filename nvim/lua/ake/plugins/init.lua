local lazypath = vim.fn.stdpath('data') .. '/lazy/lazy.nvim'

if not vim.loop.fs_stat(lazypath) then
  vim.fn.system({
    'git',
    'clone',
    '--filter=blob:none',
    'https://github.com/folke/lazy.nvim.git',
    '--branch=stable', -- latest stable release
    lazypath,
  })
end

vim.opt.rtp:prepend(lazypath)

require('lazy').setup({
  'stevearc/dressing.nvim',
  'tpope/vim-surround',
  'tpope/vim-eunuch',
  require('ake/plugins/null-ls'),
  require('ake/plugins/notify'),
  require('ake/plugins/themes/catppuccin'),
  require('ake/plugins/lir'),
  require('ake/plugins/telescope'),
  require('ake/plugins/treesitter'),
  require('ake/plugins/colorizer'),
  require('ake/plugins/winresizer'),
  require('ake/plugins/comment'),
  require('ake/plugins/lualine'),
  require('ake/plugins/neogit'),
  require('ake/plugins/gitsigns'),
  require('ake/plugins/boole'),
  require('ake/plugins/noice'),
  require('ake/plugins/vim-test'),
  require('ake/plugins/lsp'),
  require('ake/plugins/zen-mode'),
})
