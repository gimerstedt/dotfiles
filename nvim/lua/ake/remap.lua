local util = require('ake/util')
local shell = os.getenv('SHELL') or ''
local term = 'term://' .. shell

if util.is_windows() then
  term = '| term'
end

local function open(cmd)
  return "<cmd>execute '" .. cmd .. " ' .. expand('%:p:h')<cr>"
end

local function mm(mode, prefix)
  return function(lhs, rhs, opts)
    vim.keymap.set(mode, (prefix or '') .. lhs, rhs, opts)
  end
end

local i = mm('i')
local n = mm('n')
local nl = mm('n', '<leader>')
local v = mm('v')
local vl = mm('v', '<leader>')
local t = mm('t')
local ts = function(arg)
  return function()
    util.if_module('telescope.builtin', function(m)
      m[arg]()
    end)
    --    require('telescope.builtin')[arg]()
  end
end

i('jk', '<esc>')
i('<c-n>', '<c-x><c-o>')

n('Q', '@q')
n('<a-r>', ':WinResizerStartResize<cr>')
n('-', open('e'))
n('H', open('e'))

n('<a-u>', ':vsp ' .. term .. '<cr>')
n('<a-v><a-u>', ':sp ' .. term .. '<cr>')
n('<esc>', function()
  vim.api.nvim_command('nohlsearch')
  util.if_module('notify', function(notify)
    notify.dismiss()
  end)
end, { silent = true })
n('<c-n>', '*')
n('<c-p>', '#')
n('<c-t>', ':tabnew .<cr>')
n('<c-h>', ':tabp<cr>')
n('<c-l>', ':tabn<cr>')

-- change window
n('<a-h>', '<c-w>h')
n('<a-j>', '<c-w>j')
n('<a-k>', '<c-w>k')
n('<a-l>', '<c-w>l')
-- n('<a-h>', require("tmux").move_left)
-- n('<a-j>', require("tmux").move_bottom)
-- n('<a-k>', require("tmux").move_top)
-- n('<a-l>', require("tmux").move_right)

-- move window
n('<a-H>', '<c-w>H')
n('<a-J>', '<c-w>J')
n('<a-K>', '<c-w>K')
n('<a-L>', '<c-w>L')

n('gh', vim.lsp.buf.hover)
n('gr', ts('lsp_references'))
nl('ge', ts('diagnostics'))
nl('ca', vim.lsp.buf.code_action)
nl('re', vim.lsp.buf.rename)
n('ge', vim.diagnostic.goto_next)
n('gE', vim.diagnostic.goto_prev)
nl('l', function()
  vim.lsp.buf.format({ async = true })
end)
-- n('gd', vim.lsp.buf.definition)
-- n('gd', ts('lsp_definitions'))
-- n('gt', vim.lsp.buf.type_definition)
-- n('gi', vim.lsp.buf.implementation)
-- n('gr', vim.lsp.buf.references)

-- TODO: move to set up typescript plugin
-- nl('oi', function()
--   require('typescript').actions.organizeImports()
-- end)
-- nl('ia', function()
--   require('typescript').actions.addMissingImports()
-- end)

nl('bb', '<cmd>Telescope buffers<cr>')
nl('bd', '<cmd>bdelete<cr>')
nl('bn', '<cmd>bnext<cr>')
nl('bp', '<cmd>bprev<cr>')
nl('`', ':e #<cr>')

nl('fed', ':e $MYVIMRC<cr>')
nl('fep', ':e $HOME/.config/nvim/lua/ake/plugins/init.lua<cr>')
nl('ff', '<cmd>Telescope find_files<cr>')
nl('fr', '<cmd>Telescope oldfiles<cr>')
nl('fs', ':update<cr>')
nl('fg', '<cmd>Telescope live_grep<cr>')
nl('fb', '<cmd>Telescope current_buffer_fuzzy_find<cr>')

nl('te', '<cmd>term ' .. shell .. '<cr>')
nl('tn', '<cmd>TestNearest<cr>')
nl('tt', '<cmd>TestFile<cr>')
nl('tj', util.toggle_test_file)

nl('vsp', open('vsp'))
nl('sp', open('sp'))

nl('pc', '<cmd>PackerCompile<cr>')
nl('ps', '<cmd>PackerSync<cr>')

nl('q', ':q<cr>')

nl('<leader>', ':CommentToggle<cr>j')

nl('gs', '<cmd>Neogit<cr>')
nl('zz', '<cmd>ZenMode<cr>')

-- change window
t('<a-h>', util.t('<C-\\><C-N><c-w>h'))
t('<a-j>', util.t('<C-\\><C-N><c-w>j'))
t('<a-k>', util.t('<C-\\><C-N><c-w>k'))
t('<a-l>', util.t('<C-\\><C-N><c-w>l'))

-- move window
t('<a-H>', util.t('<C-\\><C-N><c-w>H'))
t('<a-J>', util.t('<C-\\><C-N><c-w>J'))
t('<a-K>', util.t('<C-\\><C-N><c-w>K'))
t('<a-L>', util.t('<C-\\><C-N><c-w>L'))

vl('l', ':Format<cr>')

vl('<leader>', ':CommentToggle<cr>')
vl('ca', vim.lsp.buf.code_action)

v('J', ":m '>+1<cr>gv=gv")
v('K', ":m '<-2<cr>gv=gv")

-- TODO: add harpoon
-- n('<header>ha', require('harpoon.mark').addfile)
-- n('<header>hi', require('harpoon.ui').toggle_quick_menu)
