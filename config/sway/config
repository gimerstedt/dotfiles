# {{{ variables
set $mod Mod4

set $left h
set $down j
set $up k
set $right l

set $term konsole
set $menu dmenu_run

set $browser1 firefox
set $browser2 chromium

# set $font DejavuSansMono
set $font FiraSansMono
set $font_size 12

# set $wallpaper /usr/share/backgrounds/sway/Sway_Wallpaper_Blue_1920x1080.png
set $wallpaper /home/ake1/.local/share/wallpaper.png

# {{{ gruvbox
set $color_foreground       #d5c4a1
set $color_foreground_bold  #ebdbb2
set $color_cursor           #ebdbb2
set $color_background       #282828

# Black, Gray, Silver, White
set $color0   #282828
set $color8   #665c54
set $color7   #d5c4a1
set $color15  #fbf1c7

# Red
set $color1   #fb4934
set $color9   #fb4934

# Green
set $color2   #b8bb26
set $color10  #b8bb26

# Yellow
set $color3   #fabd2f
set $color11  #fabd2f

# Blue
set $color4   #83a598
set $color12  #83a598

# Purple
set $color5   #d3869b
set $color13  #d3869b

# Teal
set $color6   #8ec07c
set $color14  #8ec07c

# Extra colors
set $color16  #fe8019
set $color17  #d65d0e
set $color18  #3c3836
set $color19  #504945
set $color20  #bdae93
set $color21  #ebdbb2
# }}}
# }}}
# {{{ bg
# You can get the names of your outputs by running: swaymsg -t get_outputs
output * bg $wallpaper fill
# }}}
# {{{ key bindings
bindsym $mod+u exec $term
bindsym $mod+d exec $menu
bindsym $mod+b exec $browser1
bindsym $mod+g exec $browser2
bindsym $mod+q kill
bindsym $mod+shift+c reload
bindsym $mod+shift+q exit
bindsym $mod+o layout auto next
# {{{ float modifier
# Drag floating windows by holding down $mod and left mouse button.
# Resize them with right mouse button + $mod.
# Despite the name, also works for non-floating windows.
# Change normal to inverse to use left mouse button for resizing and right
# mouse button for dragging.
floating_modifier $mod normal
# }}}
# {{{ shift focus/move
bindsym $mod+$left focus left
bindsym $mod+$down focus down
bindsym $mod+$up focus up
bindsym $mod+$right focus right

bindsym $mod+shift+$left move left
bindsym $mod+shift+$down move down
bindsym $mod+shift+$up move up
bindsym $mod+shift+$right move right
# }}}
# {{{ workspaces
bindsym $mod+1 workspace 1
bindsym $mod+2 workspace 2
bindsym $mod+3 workspace 3
bindsym $mod+4 workspace 4
bindsym $mod+5 workspace 5
bindsym $mod+6 workspace 6
bindsym $mod+7 workspace 7
bindsym $mod+8 workspace 8
bindsym $mod+9 workspace 9
bindsym $mod+0 workspace 10

bindsym $mod+shift+1 move container to workspace 1
bindsym $mod+shift+2 move container to workspace 2
bindsym $mod+shift+3 move container to workspace 3
bindsym $mod+shift+4 move container to workspace 4
bindsym $mod+shift+5 move container to workspace 5
bindsym $mod+shift+6 move container to workspace 6
bindsym $mod+shift+7 move container to workspace 7
bindsym $mod+shift+8 move container to workspace 8
bindsym $mod+shift+9 move container to workspace 9
bindsym $mod+shift+0 move container to workspace 10
# }}}
# {{{ layout switching
bindsym $mod+s layout stacking
bindsym $mod+w layout tabbed
bindsym $mod+e layout toggle split
# }}}
# {{{ misc split/fullscreen/floating
bindsym $mod+y splith
bindsym $mod+v splitv
bindsym $mod+m fullscreen
bindsym $mod+space focus mode_toggle
bindsym $mod+shift+space floating toggle
# move focus to the parent container
bindsym $mod+a focus parent
# }}}
# {{{ scratchpad:
# Move the currently focused window to the scratchpad
bindsym $mod+shift+minus move scratchpad
# Show the next scratchpad window or hide the focused scratchpad window.
# If there are multiple scratchpad windows, this command cycles through them.
bindsym $mod+minus scratchpad show
# }}}
# }}}
# {{{ resizing containers
mode "resize" {
    bindsym $left resize shrink width 10 px or 10 ppt
    bindsym $down resize grow height 10 px or 10 ppt
    bindsym $up resize shrink height 10 px or 10 ppt
    bindsym $right resize grow width 10 px or 10 ppt

    bindsym Return mode "default"
    bindsym Escape mode "default"
}
bindsym $mod+r mode "resize"
# }}}
# {{{ status bar
bar {
    position bottom
    separator_symbol " | "
    status_command i3status
    font $font $font_size
    colors {
        background $color_background
        statusline $color_foreground
        separator $color8
        focused_workspace $color16 $color16 $color_background
        inactive_workspace $color_background $color_background $color_foreground
        urgent_workspace $color_background $color_background $color_foreground
    }
}
# }}}
# {{{ border
default_border pixel 3
hide_edge_borders smart
# }}}
# {{{ border colors
# border, background, text, indicator, child_border
# client.background $color16 $color16 $color16 $color16 $color16
client.background $color16
client.focused $color16 $color16 $color16 $color16 $color16
client.focused_inactive $color_background $color_background $color_background $color_background $color_background
client.unfocused $color_background $color_background $color_background $color_background $color_background
client.urgent $color_background $color_background $color_background $color_background $color_background
client.placeholder $color16 $color16 $color16 $color16 $color16
# }}}
include /etc/sway/config.d/*
# vim: foldmethod=marker
