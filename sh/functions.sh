# not in use
# not in use
# not in use

if [[ -x $(which docker 2>/dev/null) ]]; then
  function dstats { docker stats $(docker ps --format \"{{.Names}}\"); }
  function drmexited { docker rm $(docker ps -aqf status=exited); }
  function drmstopped { docker rm $(docker ps -aq); }
  function drmiuntagged { docker rmi $(docker images -q --filter "dangling=true"); }
fi

if [[ -x $(which git 2>/dev/null) ]]; then
  function gcam()
  {
    git add .
    git commit -m "$@"
    git pull --rebase
  }
fi
