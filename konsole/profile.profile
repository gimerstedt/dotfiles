[Appearance]
ColorScheme=Gruvbox_dark
Font=Fira Code,10,-1,5,50,0,0,0,0,0,Regular
UseFontLineChararacters=true

[General]
Name=profile
Parent=FALLBACK/
ShowTerminalSizeHint=false

[Interaction Options]
AutoCopySelectedText=false

[Scrolling]
ScrollBarPosition=2
