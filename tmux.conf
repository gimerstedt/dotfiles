# TODO: migrate to tmux/tmux.conf
set -g default-terminal "screen-256color"
set -g mouse on
set -g visual-activity on
set -g history-limit 10000
set -g @shell_mode 'vi'

setw -g automatic-rename
setw -g mode-keys vi

bind R source-file ~/.tmux.conf

# nav panes
bind h select-pane -L
bind j select-pane -D
bind k select-pane -U
bind l select-pane -R

# nav windows
bind -r C-h select-window -t :-
bind -r C-l select-window -t :+

# v/y to begin/stop selection in cop mode
bind-key -t vi-copy 'v' begin-selection
bind-key -t vi-copy 'y' copy-selection

# resize panes with vim movement keys
bind -r H resize-pane -L 5
bind -r J resize-pane -D 5
bind -r K resize-pane -U 5
bind -r L resize-pane -R 5

bind -r C-k split-window -h
bind -r C-j split-window -v

## Status bar design
# status line
#set -g status-justify left
#set -g status-bg default
#set -g status-fg colour12
#set -g status-interval 2

# messaging
# set -g message-fg black
# set -g message-bg yellow
# set -g message-command-fg blue
# set -g message-command-bg black

# window mode
setw -g mode-bg colour6
setw -g mode-fg colour0

# window status
setw -g window-status-format " #F#I:#W#F "
setw -g window-status-current-format " #F#I:#W#F "
setw -g window-status-format "#[fg=magenta]#[bg=black] #I #[bg=cyan]#[fg=colour8] #W "
setw -g window-status-current-format "#[bg=brightmagenta]#[fg=colour8] #I #[fg=colour8]#[bg=colour14] #W "
setw -g window-status-current-bg colour0
setw -g window-status-current-fg colour11
setw -g window-status-current-attr dim
setw -g window-status-bg green
setw -g window-status-fg black
setw -g window-status-attr reverse

# Info on left (I don't have a session display for now)
set -g status-left ''

# loud or quiet?
set-option -g visual-activity off
set-option -g visual-bell off
set-option -g visual-silence off
set-window-option -g monitor-activity off
set-option -g bell-action none

####################
### STYLE STUFFS ###
####################

# The modes {
setw -g clock-mode-colour colour135
setw -g mode-attr bold
setw -g mode-fg colour196
setw -g mode-bg colour238

# }
# The panes {

#set inactive/active window styles
set -g window-style 'fg=default,bg=colour239'
set -g window-active-style 'fg=default,bg=black'

# set the pane border colors
set -g pane-border-fg black
set -g pane-border-bg black
set -g pane-active-border-fg colour208
set -g pane-active-border-bg colour208
# }
# The statusbar {

set -g status-position bottom
set -g status-bg default
set -g status-fg colour137
set -g status-attr dim
set -g status-left ''
set -g status-right '#[fg=colour233,bg=colour241,bold] %d/%m #[fg=colour233,bg=colour245,bold] %H:%M:%S '
set -g status-right-length 50
set -g status-left-length 20

setw -g window-status-current-fg colour208
setw -g window-status-current-bg black
# setw -g window-status-current-attr bold
setw -g window-status-current-format ' #I#[fg=colour250]:#[fg=colour208]#W#[fg=colour50]#F '

setw -g window-status-fg colour138
setw -g window-status-bg colour238
setw -g window-status-attr none
setw -g window-status-format ' #I#[fg=colour237]:#[fg=colour250]#W#[fg=colour244]#F '

setw -g window-status-bell-attr bold
setw -g window-status-bell-fg colour255
setw -g window-status-bell-bg colour1

# }
# The messages {

set -g message-attr bold
set -g message-fg colour232
set -g message-bg colour166

# }
