# enable vi mode and set delay to 100ms
fish_vi_key_bindings
set fish_escape_delay_ms 100
fish_hybrid_key_bindings for 2.5
set fish_cursor_insert block

# disable greeting
set fish_greeting ""

# disable dumbass mode indicator in prompt - like seriously...who would want this?
function fish_mode_prompt
end

# source misc {{{

# source env file
source ~/.config/fish/misc/env.fish

# source coloring helper functions
source ~/.config/fish/misc/color_helpers.fish

# source aliases file
source ~/.config/fish/misc/aliases.fish

# source path file
source ~/.config/fish/misc/path.fish

# source local config
test -f ~/.config/fish/local.config.fish
and source ~/.config/fish/local.config.fish

# source ~/.config/fish/misc/tokyonight.fish
# source ~/.config/fish/misc/duskfox.fish

# }}}

# vim: foldmethod=marker
