set -x -g EDITOR nvim
set -x -g VISUAL $EDITOR
set -x -g GIT_EDITOR $EDITOR

set -x -g XDG_DATA_HOME "$HOME/.local/share"
set -x -g XDG_CONFIG_HOME "$HOME/.config"
set -x -g XDG_STATE_HOME "$HOME/.local/state"
set -x -g XDG_CACHE_HOME "$HOME/.cache"

set -x -g NODE_HOME ~/.local/lib/node
set -x -g CARGO_HOME $XDG_DATA_HOME/cargo

# TODO: make rg read .ignore
set -x -g FZF_DEFAULT_COMMAND 'rg --no-messages --files --hidden --follow --glob "!{.git,archive,unrar,torrents,.mirror}"'

test -f "$HOME/.ca/bundle.pem"
and set -x -g NODE_EXTRA_CA_CERTS "$HOME/.ca/bundle.pem"

# https://github.com/catppuccin/bat
set -x -g BAT_THEME "Catppuccin-mocha"

set -x -g EXA_ICON_SPACING 2
