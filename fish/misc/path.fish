# fish_add_path once 3.2
function _ap
  contains $argv $fish_user_paths; or set -Ua fish_user_paths $argv
end

_ap /sbin
_ap "$CARGO_HOME/bin"
_ap "$NODE_HOME/node_modules/.bin"
_ap ~/.local/bin
