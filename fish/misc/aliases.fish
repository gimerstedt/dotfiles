# shell
alias ..='cd ..'
alias ...='cd ../..'
alias ....='cd ../../..'
alias getclip='xclip -selection clipboard -o'
alias setclip='xclip -selection clipboard'

# alias n='npm'
# alias y='yarn'
# alias p='pnpm'

if has bat
  alias cat='bat'
end

if has htop
  alias top='htop'
end

if has btm
  alias top='btm -b'
end

if has dbwebb
  alias d='dbwebb'
end
