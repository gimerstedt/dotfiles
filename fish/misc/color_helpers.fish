function pcyan
    printf (set_color cyan)$argv(set_color normal)
end

function pyellow
    printf (set_color yellow)$argv(set_color normal)
end

function pred
    printf (set_color red)$argv(set_color normal)
end

function pblue
    printf (set_color blue)$argv(set_color normal)
end

function pgreen
    printf (set_color green)$argv(set_color normal)
end

function ppurple
    printf (set_color purple)$argv(set_color normal)
end

function ecyan
    echo (pcyan $argv)
end

function eyellow
    echo (pyellow $argv)
end

function ered
    echo (pred $argv)
end

function eblue
    echo (pblue $argv)
end

function egreen
    echo (pgreen $argv)
end

function epurple
    echo (ppurple $argv)
end
