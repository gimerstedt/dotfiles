function on-path -d 'Check if argv is on PATH'
    for i in (seq 1 (count $PATH))
        if test $PATH[$i] = $argv
            return 0
        end
    end
    return 1
end
