function ip
    set _bin (which ip)
    if test (count $argv) -eq 0
        $_bin -c -br a
    else
        $_bin -c $argv
    end
end
