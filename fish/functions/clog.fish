function clog -d 'clog [container name]'
    if test (count $argv) -gt 1
        journalctl CONTAINER_NAME=$argv[1] $argv[2..-1]
    else if test (count $argv) -gt 0
        journalctl CONTAINER_NAME=$argv[1]
    else
        ered "fel"
        return 1
    end
end
