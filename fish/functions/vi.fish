function vi
    set -l arg $argv
    if test (count $argv) -eq 0
      set arg .
    end

    if has nvim
        nvim $arg
    else
        vim $arg
    end
end
