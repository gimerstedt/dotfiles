function lag -d 'grep ls -al results'
    if test (count $argv) -gt 1
        ls -al $argv[1] | grep $argv[2..-1]
    else if test (count $argv) -gt 0
        ls -al | grep $argv
    else
        ered 'Need argument for grep'
        return 1
    end
end
