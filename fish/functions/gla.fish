function gla
    for d in *
        cd $d
        if git status | grep -q "nothing to commit"
            echo "========================"
            echo "Current directory: $d"
            gl
        end
        cd ..
    end
end
