function nginx -d 'nginx [path] [port]'
    if not has docker
        ered 'docker not on path'
        return 1
    end

    set -l dir
    set -l name
    set -l port 80

    # make sure path is usable and a valid name
    if test (count $argv) -eq 0
        or test $argv[1] = '.'
        set dir (pwd)
        set name (pwd | sed 's/\//S/g')
    else if test -d $argv[1]
        set dir $argv[1]
        set name (echo $argv[1] | sed 's/\//S/g')
    else
        ered 'faulty arguments'
        return 1
    end

    # override port
    if test (count $argv) -eq 2
        set port $argv[2]
    end

    docker run --rm -it -p $port:80 --name $name -v $dir:/usr/share/nginx/html:ro nginx:alpine
end
