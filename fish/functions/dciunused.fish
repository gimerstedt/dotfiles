function dciunused -d 'Remove week old images not in use'
    if not has docker
        ered 'docker not on path'
        return 1
    end
    set -l format '{{.ID}} {{.CreatedSince}}'
    set -l rep_tag_age '{{.Repository}}:{{.Tag}} {{.CreatedSince}}'
    set -l old 'weeks\|months'
    set -l firstcol '{ print $1 }'

    set -l no_old_before (count (docker images --format $format | grep $old))
    docker rmi (docker images --format $rep_tag_age | grep $old | awk $firstcol) ^/dev/null
    docker rmi (docker images --no-trunc --format $format | grep $old | awk $firstcol) ^/dev/null
    set -l no_old_after (count (docker images --format $format | grep $old))

    test $no_old_before -eq $no_old_after
    and egreen 'No week old images not in use exists'
end
