function g
    if test -f gradlew
        ./gradlew $argv
    else
        gradle $argv
    end
end
