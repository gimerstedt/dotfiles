if has exa
    complete -c la -w exa
    function la
        exa -la --time-style=iso --icons --git $argv
    end
else
    complete -c la -w ls
    function la
        ls -la $argv
    end
end
