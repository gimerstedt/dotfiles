function ake -d 'Manage packages'
    if test (count $argv) -eq 0
        __ake_help
        return 1
    end

    set op $argv[1]
    if test (count $argv) -gt 1
        set rest $argv[2..-1]
    end

    switch $op
        case i install
            __ake_install $rest
        case s search
            __ake_search $rest
        case r rm remove
            __ake_remove $rest
        case u
            __ake_update $rest
            ; and __ake_upgrade $rest
            ; and __ake_clean $rest
            # case up update
            #     __ake_update $rest
            # case upg upgrade
            #     __ake_upgrade $rest
            # case c clean
            #     __ake_clean $rest
        case l local
            __ake_local $rest
        case y yarn n node npm
            __ake_node $rest
        case '*'
            __ake_help
            return 1
    end
end

# {{{ local
function __ake_local
    if test (count $argv) -eq 2
        set bin $argv[1]
        set name $argv[2]
        set dir ~/.local/bin
        mkdir -p $dir
        if not test -f $bin
            echo $bin does not exist.
            return 1
        end
        if begin test -f $dir/$name
                or test -L $dir/$name
            end
            ls -al $dir/$name
            echo $dir/$name already exists.
            return 1
        end
        ln -s $bin $dir/$name
        echo $bin linked as $dir/$name
    else
        echo Usage: ake l \<path to binary\> name
        return 1
    end
end
# }}}
# {{{ help
function __ake_help
    echo Wrapper for misc package managers, use at your own peril
    echo ""
    echo Usage:
    echo \take \<op\> [args]
    echo Ops:
    echo \ti/in/install\t\tInstall [args]
    echo \ts/search\t\tSearch for [args]
    echo \tr/rm/remove\t\tUninstall [args]
    echo \tu/uu\t\t\tUpdate, upgrade and clean up
    # echo \tup/update\t\tUpdate packages
    # echo \tupg/upgrade\t\tUpgrade packages
    # echo \tc/clean\t\t\tClean up
end
# }}}
# {{{ install
function __ake_install
    if has yay
        yay -S $argv
    else if has yaourt
        yaourt -S $argv
    else if has pacman
        sudo pacman -S $argv
    else if has apt
        sudo apt install $argv
    else if has apt-get
        sudo apt-get install $argv
    end
end
# }}}
# {{{ search
function __ake_search
    if has yay
        yay -Ss $argv
    else if has yaourt
        yaourt -Ss $argv
    else if has pacman
        pacman -Ss $argv
    else if has apt
        apt search $argv
    else if has apt-get
        apt-cache search $argv
    else if has nix-env
        nix-env -qaP ".*$argv.*"
    end
end
# }}}
# {{{ remove
function __ake_remove
    if has yay
        yay -Rns $argv
    else if has yaourt
        yaourt -Rns $argv
    else if has pacman
        pacman -Rns $argv
    else if has apt
        sudo apt remove $argv
    else if has apt-get
        sudo apt-get remove $argv
    end
end
# }}}
# {{{ update
function __ake_update
    # if has yay
    #     yay -Sy
    # else if has yaourt
    #     yaourt -Sy
    # else if has pacman
    #     sudo pacman -Sy
    if has apt
        sudo apt update $argv
    else if has apt-get
        sudo apt-get update $argv
    end
end
# }}}
# {{{ upgrade
function __ake_upgrade
    if has yay
        yay -Syu
    else if has yaourt
        yaourt -Syua
    else if has pacman
        sudo pacman -Syu
    else if has apt
        sudo apt upgrade $argv
    else if has apt-get
        sudo apt-get upgrade $argv
    end; or return 1

    if functions -q fisher
      fisher update
    end; or return 1

    if has npm
        ake y u
    end; or return 1
end
# }}}
# {{{ clean
function __ake_clean
    if has pacman
        set -l garbage (pacman -Qtdq)
        if test -n "$garbage"
            sudo pacman -Rs $garbage
        end
    else if has apt
        sudo apt-get autoremove $argv
    else if has apt-get
        sudo apt-get autoremove $argv
    end
end
# }}}
# {{{ node
function __ake_node
    if not has npm
        ered "npm not on your \$PATH"
        return 1
    end

    set op $argv[1]
    if test (count $argv) -gt 1
        set rest $argv[2..-1]
    end

    if set -q $NODE_HOME
        ered "NODE_HOME env var not set"
        return 1
    end

    switch $op
        case i
            npm --prefix "$NODE_HOME" i $rest
        case r rm
            npm --prefix "$NODE_HOME" rm $rest
        case u
            npm --prefix "$NODE_HOME" upgrade
        case ls list
            npm --prefix "$NODE_HOME" ls
        case ll la
            npm --prefix "$NODE_HOME" ll
        case cd
            cd "$NODE_HOME"
        case s
            __npm_search $rest all
        case '*'
            return 1
    end
end

function __npm_search
    set search_term $argv[1]
    if test (count $argv) -gt 2
        set additional_query_params "&hitsPerPage=10&page=0"
    else
        set additional_query_params ""
    end
    set url 'https://ofcncog2cu-dsn.algolia.net/1/indexes/*/queries?x-algolia-application-id=OFCNCOG2CU&x-algolia-api-key=f54e21fa3a2a0160595bb058179bfb1e'
    set data "{\"requests\":[{\"indexName\":\"npm-search\",\"params\":\"query=$search_term$additional_query_params&attributesToRetrieve=%5B%22description%22%2C%22name%22%2C%22version%22%5D&attributesToHighlight=%5B%5D\"}]}"
    curl -s $url --compressed -H 'Accept: application/json' -H 'content-type: application/x-www-form-urlencoded' -H 'Pragma: no-cache' -H 'Cache-Control: no-cache' --data $data | jq -r '.results[].hits[] | .name + "@" + .version + "|" + .description' | column -t -s '|' | cut -c-$COLUMNS
end
# }}}
# vim: foldmethod=marker
