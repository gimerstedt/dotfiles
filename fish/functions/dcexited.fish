function dcexited -d 'Remove exited containers'
    if not has docker
        ered 'docker not on path'
        return 1
    end
    test (count (docker ps -aqf status=exited)) -gt 0
    and docker rm (docker ps -aqf status=exited)
    or egreen 'No exited containers exists'
end
