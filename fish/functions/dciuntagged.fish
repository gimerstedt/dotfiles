function dciuntagged -d 'Remove untagged images'
    if not has docker
        ered 'docker not on path'
        return 1
    end
    test (count (docker images -qf dangling=true)) -gt 0
    and docker rmi (docker images -qf dangling=true)
    or egreen 'No untagged images exists'
end
