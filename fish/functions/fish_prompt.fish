# vim: foldmethod=marker
# {{{ print helper
function __print_color
    set -l color $argv[1]
    set -l string $argv[2]

    set_color $color
    printf $string
    set_color normal
end
# }}}

function fish_prompt -d "the prompt"
    # echo -e ""

    # {{{ user
    set -l user (id -un $USER)
    __print_color normal "$user"
    # }}}

    # {{{ host
    set -l host_name (hostname -s)
    set -l host_glyph "@"

    __print_color normal "$host_glyph"
    __print_color normal "$host_name"
    # }}}

    # {{{ current working directory
    set -l pwd_glyph " "
    set -l pwd_string (echo $PWD | sed 's|^'$HOME'\(.*\)$|~\1|')

    __print_color normal "$pwd_glyph"
    __print_color normal "$pwd_string"
    # }}}

    # {{{ prompt
    set -l prompt_glyph '$'
    set -l prompt_color 'A78BFA'
    # set -l prompt_color 'F57F17'
    if test (whoami) = 'root'
        set prompt_glyph '#'
        set prompt_color 'f7768e'
    end

    __print_color $prompt_color "\e[K\n$prompt_glyph "
    # }}}
end
