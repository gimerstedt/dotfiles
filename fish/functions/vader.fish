function vader -d 'Shows the weather'
    test (count $argv) -eq 0
    and __wttr "karlskrona"
    or __wttr $argv
end

function __wttr
    curl -s "wttr.in/$argv" | grep -v "Follow\|Feature\|City"
end
