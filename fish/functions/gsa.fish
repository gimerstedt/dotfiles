function gsa
    for d in *
        cd $d
        if not git status | grep -q "nothing to commit"
            echo "========================"
            echo "Current directory: $d"
            git status
        end
        cd -
    end
end
