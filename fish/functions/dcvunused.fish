function dcvunused -d 'Remove unused volumes'
    if not has docker
        ered 'docker not on path'
        return 1
    end
    test (count (docker volume ls -qf dangling=true)) -gt 0
    and docker volume rm (docker volume ls -qf dangling=true)
    or egreen 'No unused volumes exists'
end
