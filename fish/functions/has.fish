function has -d 'Check if binary exists'
    if which $argv &> /dev/null
        return 0
    end
    return 1
end
