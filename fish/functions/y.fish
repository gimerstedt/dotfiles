function y
    if test -f "pnpm-lock.yaml"
        pnpm $argv
    else if test -f "yarn.lock"
        yarn $argv
    else
        npm $argv
    end
end
