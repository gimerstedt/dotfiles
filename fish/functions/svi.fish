function svi
    if has nvim
        sudo -E nvim $argv
    else
        sudo -E vim $argv
    end
end
