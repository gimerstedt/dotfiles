function dps
    test (count $argv) -eq 0
    and docker ps --all --format 'table {{.Names}}\t{{.Status}}\t{{.Ports}}'
    or docker ps -a
end
