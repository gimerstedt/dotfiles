function dcall -d 'Remove all exited containers, untagged and week old unused image and unused volumes'
    if not has docker
        ered 'docker not on path'
        return 1
    end
    if test (count $argv) -eq 0
        ered 'Supply ANY argument to execute'
        return 1
    else
        dcexited
        dciuntagged
        dciunused
        dcvunused
    end
end
