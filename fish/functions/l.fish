if has exa
    complete -c l -w exa
    function l
        # exa -l --no-time --no-permissions --octal-permissions --icons --git $argv
        exa -l --no-time --no-permissions --octal-permissions --icons $argv
    end
else
    complete -c l -w ls
    function l
        ls -hl $argv
    end
end
