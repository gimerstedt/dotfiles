if has exa
    complete -c lrt -w exa
    function lrt
        exa -ls modified --git $argv
    end
else
    complete -c lrt -w ls
    function lrt
        ls -hlrt $argv
    end
end
