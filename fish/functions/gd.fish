function gd
  if has bat
    git diff $argv | bat
  else
    git diff $argv
  end
end
