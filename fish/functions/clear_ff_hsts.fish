complete -d 'Clear firefox HSTS'
function clear_ff_hsts
    if test (pgrep firefox)
        echo 'Close Firefox before attempting to clear HSTS.'
        return 1
    end
    truncate -s 0 ~/.mozilla/firefox/**/SiteSecurityServiceState.txt
end
