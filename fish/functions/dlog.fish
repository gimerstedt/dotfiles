complete --command dlog --description "Container" --no-files --arguments "(sudo docker ps --all --format \"{{.Names}}\")"

function dlog -d 'Tail the last 500 lines and follow container log'
    sudo docker logs --timestamps -f --tail=500 $argv
end
