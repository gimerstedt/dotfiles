function ff
    rg --files-with-matches --no-messages "$argv" | fzf $FZF_PREVIEW_WINDOW --preview "rg --ignore-case --pretty --context 10 '$argv' {}"
end
