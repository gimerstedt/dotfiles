if has exa
    complete -c lt -w exa
    function lt
        exa -lrs modified --git $argv
    end
else
    complete -c lt -w ls
    function lt
        ls -hlt $argv
    end
end
