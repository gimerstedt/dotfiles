function __go_up
    commandline -r 'cd ..'
    commandline -f execute
end

function __go_path_1
    commandline -r 'cd $HOTKEY_PATH_1'
    commandline -f execute
end

function __go_path_2
    commandline -r 'cd $HOTKEY_PATH_2'
    commandline -f execute
end

function __list_or_exec
    if test -n (commandline -b)
        commandline -f execute
    else
        commandline -r l
        commandline -f execute
    end
end

function __node_exec
    set script (commandline -b)
    commandline -r "echo \"$script\" | node -p"
    commandline -f execute
end

function fish_user_key_bindings --description 'User defined key bindings'
    # jk as vi mode escape key
    bind -M insert -m default jk backward-char force-repaint

    # override default behavior to use 'la' if no command
    bind \cj __list_or_exec
    bind \ck __go_up
    bind \cg __go_path_1
    bind \ch __go_path_2
    bind \ce __node_exec

    bind \cj -M insert __list_or_exec
    bind \ck -M insert __go_up
    bind \cg -M insert __go_path_1
    bind \ch -M insert __go_path_2
    bind \ce -M insert __node_exec
    bind \cv -M insert vi

    # why breaking changes in point release? whyyy?
    bind \cp history-search-backward
    bind \cn history-search-forward
    bind \cf accept-autosuggestion
    bind \cp -M insert history-search-backward
    bind \cn -M insert history-search-forward
    bind \cf -M insert accept-autosuggestion
end

#fzf_key_bindings
