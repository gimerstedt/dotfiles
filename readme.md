# ocelots are amazing:wq

# usual suspects
## core/community/extra
- bat
- bottom
- exa
- fish
- git
- mpv
- nnn
- nodejs
- npm
- rustup
  - rustup install stable
  - cargo install stylua
- tmux
- yarn

## aur
nerd-fonts-jetbrains-mono
tty-clock

## neovim
```sh
mkdir -p ~/git && cd ~/git
git clone https://github.com/neovim/neovim.git
cd neovim
CMAKE_BUILD_TYPE=RelWithDebInfo make -j4
sudo make install
```

## update
```fish
#!/usr/bin/env fish

function system
  ake u
end

function neovim
  cd ~/git/neovim
  git pull # lule
  rm -rf build
  make distclean
  CMAKE_BUILD_TYPE=RelWithDebInfo make -j4
  sudo make install
end

function rust
  rustup update
end

system; and neovim; and rust
```
